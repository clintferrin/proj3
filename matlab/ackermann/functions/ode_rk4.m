%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function xnew = ode_rk4(diffeq,xold,ytilde,u,params)

h=params.rk4.dt;
hh = h/2;
h6 = h/6;

y = xold;
dydx = feval(diffeq,y,ytilde,u,params);
yt = y + hh*dydx;

dyt = feval(diffeq,yt,ytilde,u,params);
yt = y +hh*dyt;

dym = feval(diffeq,yt,ytilde,u,params);
yt = y +h*dym;
dym = dyt + dym;

dyt = feval(diffeq,yt,ytilde,u,params);
yout = y + h6*(dydx+dyt+2*dym);
xnew = yout;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end