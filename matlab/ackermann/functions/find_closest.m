function [min_y_error, min_idx] = find_closest(pt_pos,ref_traj,idx)
    if idx > length(ref_traj(1,:)) || idx < 1
        output="Invalid entry"
    end
    
    x_diff = diff(ref_traj(1,:));
    y_diff = diff(ref_traj(2,:));
    derivative = atan2(y_diff,x_diff);
    
    direction = 1;
    min_x_error = inf;
    min_y_error = inf;
   
    min_found = 0;

    while min_found == 0 && idx > 0 && idx <= length(ref_traj(1,:))
        [x_error, y_error] = find_2d_errors(pt_pos, ref_traj(:,idx), derivative(idx));
        if abs(x_error) + abs(y_error) <= abs(min_x_error) + abs(min_y_error)
            min_x_error = x_error;
            min_y_error = y_error;
            min_idx = idx;
%             scatter(ref_traj(1,idx),ref_traj(2,idx));
        elseif direction == 1
            direction = -1;
            idx = idx + direction;
        else
            min_found = 1;
        end
        idx = idx + direction;
    end
    min_x_error;
end
