import pandas as pd


def main():
    desired_charge = 60  # Watts
    sim = ChargingSimulation("charging_data.csv", desired_charge=desired_charge, Q_max=8)
    sim.write_P_out_values("output.csv")


class ChargingSimulation:
    def __init__(self, file_path, desired_charge=60, Q_max=8):
        self.pandas = pd.read_csv(file_path)
        self.numpy = self.pandas.values
        self.n = self.numpy.shape[0]
        self.variations = []

        self.desired_charge = desired_charge
        self.Q_max = Q_max

        # build varition type objects
        for variation in self.numpy:
            # calculate Q
            self.variations.append(self.__Variation(variation))

    def write_P_out_values(self, file_path):
        f = open(file_path, "w")

        # write out headers
        f.write("variation_num,angel_deg,x_dis,y_dis,k_total,l_receiver,Q_calculated,Q_actual,P_out\n")

        # write individual fields
        for variation in self.variations:
            # print(variation.number)
            f.write(str(variation.number) + "," +
                    str(variation.angle_deg) + "," +
                    str(variation.x_dis) + "," +
                    str(variation.y_dis) + "," +
                    str(variation.k_total) + "," +
                    str(variation.l_receiver) + "," +
                    str(variation.Q_calculated) + "," +
                    str(variation.Q_actual) + "," +
                    str(variation.P_out) + "\n")

    class __Variation:
        def __init__(self, data, desired_charge=60, Q_max=8):
            self.data = data
            self.number = int(data[0])
            self.angle_deg = float(data[1][:-3])
            self.x_dis = float(data[2][:-2]) * 10e-3
            self.y_dis = float(data[3][:-2]) * 10e-3
            self.k_t1_to_receiver = float(data[4])
            self.k_t2_to_receiver = float(data[6])
            self.k_t3_to_receiver = float(data[8])
            self.k_t4_to_receiver = float(data[10])

            k_t1_to_receiver = self.k_t1_to_receiver
            k_t2_to_receiver = self.k_t2_to_receiver
            k_t3_to_receiver = self.k_t3_to_receiver
            k_t4_to_receiver = self.k_t4_to_receiver

            self.l_receiver = float(data[24][:-3]) * 10e-9
            self.l_t1 = float(data[26][:-3]) * 10e-9
            self.l_t2 = float(data[28][:-3]) * 10e-9
            self.l_t3 = float(data[30][:-3]) * 10e-9
            self.l_t4 = float(data[32][:-3]) * 10e-9

            # set default values
            desired_charge = desired_charge  # set to 60
            w = 5  # I am no sure what w is from description
            I = 250

            # calculate Q
            k = k_t1_to_receiver + k_t2_to_receiver + k_t3_to_receiver + k_t4_to_receiver
            L = self.l_receiver
            Q_calculated = desired_charge / (w * L * I * I * k * k)

            # check if Q_calculated is within limits
            if Q_calculated > Q_max:
                P_out = w * L * I * I * k * k * Q_max
                Q_actual = Q_max
            else:
                P_out = desired_charge
                Q_actual = Q_calculated

            self.k_total = k
            self.Q_calculated = Q_calculated
            self.P_out = P_out
            self.Q_actual = Q_actual


if __name__ == '__main__':
    main()
