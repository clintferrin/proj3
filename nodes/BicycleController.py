#! /usr/bin/env python
# coding=utf-8

from numpy import eye, array, c_, r_, array, diag, cos, sin, tan
import Parameters as pm
import VehicleSim as ve
import rospy
import geometry_msgs.msg as geo
import numpy as np
from vehicle_sim.msg import vehicle_inputs, bicycle_state
from utils import rk4


def main():
    # initialization
    params = pm.Parameters()
    sim = ve.VehicleSim(params)
    controller = BicycleController(params)
    controller.publish_control_accel()


class BicycleController:
    def __init__(self, params):
        self.eps = params.control_reference
        self.L = params.wheel_base
        self.dt = 0.01
        self.t = 0
        self.x_state = array([[0], [0], [0], [0], [0]])

        # subscribe to model
        self.stamp = []
        # rospy.init_node('controller', anonymous=True)
        # rospy.Subscriber('x_state', bicycle_state, self.__x_update)

        # # create desired trajectory
        # self.yd, self.yd_dot, self.yd_ddot = self.build_trajectory()

        # initialize matrix variables
        I = eye(2)

        Z = array([[0, 0],
                   [0, 0]])

        A = r_[c_[Z, I],
               c_[Z, Z]]

        B = r_[Z,
               I]

        # initialize lqr controller variables
        x_pos = 150
        y_pos = 150
        vel = 10
        omega = 10
        Q = diag([x_pos, y_pos, vel, omega])

        accel = 1
        ang_accel = 1
        R = diag([accel, ang_accel])
        # self.K = lqr(A, B, Q, R)

    def __x_update(self, data):
        self.stamp = rospy.Time.now()
        self.x_state[0, 0] = data.x
        self.x_state[1, 0] = data.y
        self.x_state[2, 0] = data.psi
        self.x_state[3, 0] = data.v
        self.x_state[4, 0] = data.phi

    def __calc_u(self, x):
        R_eps = array([[cos(x[2]), -self.eps * sin(x[2])],
                       [sin(x[2]), self.eps * cos(x[2])]])

        w = x[3] * tan(x[4]) / self.L

        w_hat = array([[0, self.eps * w],
                       [w / float(self.eps), 0]])

        y_eps = x[0:3] + self.eps * array([[cos(x[2])],
                                           [sin(x[2])]])

        y_eps_dot = R_eps * array([[x[3]],
                                   [w]])

        Z = array([[y_eps], [y_eps_dot]]) - array([[self.yd[self.t]], [self.yd_dot[self.t]]])
        u = -self.yd_ddot[self.t] - self.K * Z

        a_bar = w_hat * array([[x[3]], [w]]) + np.linalg.inv(R_eps).dot(u)

        if x[3] == 0:
            xi = 0
        else:
            xi = cos(x[4]) ^ 2 * (a_bar(2) - a_bar(1) * tan(x[4])) / x[3]

        a_bar = array([[a_bar(1)], [xi]])

        return a_bar

    def calc_intput(self, x):
        u = [0, 0]
        return u

    def calc_zero_input(self, x):
        u = [0, 0]
        return u

    def build_trajectory(self):
        time = np.arange(0, 20, self.dt)
        yd = array([[time],
                    [sin(time)]])

        yd_dot = array([[np.ones(1, len(time))],
                        [cos(time)]])

        yd_ddot = array([[np.zeros(1, len(time))],
                         [-sin(time)]])

        return yd, yd_dot, yd_ddot

    def velocity_controller(self, v, vd):
        return -5 * (v - vd)

    def publish_control_accel(self):
        pub = rospy.Publisher('control_accel', vehicle_inputs, queue_size=10)

        rate = rospy.Rate(100)  # hz
        while not rospy.is_shutdown():
            x = self.x_state
            u = self.__calc_u(x)

            log_msg = "Angular Accerlation:"
            log_msg = log_msg + "   linear: " + str(self.accel[0, 0])
            log_msg = log_msg + "   angular: " + str(self.accel[1, 0])
            rospy.loginfo(log_msg)
            # pub.publish(control_accel)
            rate.sleep()


if __name__ == '__main__':
    main()
