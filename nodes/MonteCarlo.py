#! /usr/bin/env python
# coding=utf-8

from numpy import zeros
import numpy as np
from Parameters import Parameters
from VehicleSim import VehicleSim
from BicycleController import BicycleController
import KalmanFilter as kf
from SensorSuite import SensorSuite
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import time
import pickle
import datetime
import utils as ut


def main():
    # initialization
    params = Parameters()
    sensors = SensorSuite(params)
    controller = BicycleController(params)
    monte = MonteCarlo(params)
    sim = []
    observer = []

    # initialize the starting conditions for sim and observer
    for i in range(monte.num_simulations):
        # initialize simulation
        sim.append(VehicleSim(params))
        observer.append(kf.KalmanFilter(sensors, params))

    # run simulation
    for idx_sim, simulation in enumerate(monte.simulations):
        for t in range(len(simulation.time)):
            # calculate controller input
            u = controller.calc_zero_input(observer[idx_sim].x_hat)

            # propogate vehcile forward
            x = sim[idx_sim].propagate(u)

            # obtain measurments from current state
            y_tilde = [sensors.get_velocity(x),
                       sensors.get_steering_angle(x)]

            # propagate the estimated state and covariance
            x_hat, P, resid_P = observer[idx_sim].propagate(y_tilde)

            # discrete update
            if np.mod(t, round(1 / (float(params.gps_freq) * monte.dt))) == 0:
                x_hat, P, resid, resid_P = observer[idx_sim].update(sensors.get_gps_pos(x))
                simulation.resid = np.c_[simulation.resid, resid]
                simulation.resid_t = np.append(simulation.resid_t, t * monte.dt)

            # save simulation values
            simulation.x[:, :, t] = x
            simulation.x_hat[:, :, t] = x_hat
            simulation.P[:, :, t] = P
            simulation.resid_P[:, :, t] = resid_P

        # output percent complete
        monte.print_update(idx_sim)

    # calculate statistics and save output
    monte.end_routine()

    # monte.save_all_figures_tikz("tikz/", points=250)
    # monte.save_all_figures_tikz("tikz/_", points=2)
    monte.subplot_monte_carlo()
    plt.show()


class MonteCarlo:
    def __init__(self, params):
        # # plotting attributes for Latex
        # plt.rc('text', usetex=True)
        # plt.rc('font', family='serif')

        # fill simulations with zeros
        self.num_simulations = params.simulations
        self.simulation_len = params.end_time
        self.dt = params.dt  # integration time step
        self.end_time = params.end_time  # seconds
        self.time = np.arange(0, self.end_time, self.dt)
        self.error = []
        self.simulations = []
        self.error_three_sig = []
        self.residual_mean = []
        for i in range(self.num_simulations):
            self.simulations.append(MonteSim(params))
        self.start = time.time()

        # label information
        self.y_labels = ["Error $x$ Distance (m)",
                         "Error $y$ Distance (m)",
                         "Error $\Psi$ Angle (rad)",
                         "Error $b_v$ Velocity Bias (m)",
                         "Error $b_{\phi}$ Steering Angle Bias (rad)"]

    def pickle_monte_carlo_runs(self):
        stamp = datetime.datetime.now().strftime("%m%d%H%M%S")
        file_name = "simulations/sims" + str(self.num_simulations) \
                    + "-len" + str(self.end_time) \
                    + "-stamp" + stamp
        pickle.dump(self, open(file_name, "wb"))

    def load_monte_carlo(self, load_file):
        return pickle.load(open(load_file, "rb"))

    def plot_trajectory(self, idx):
        plt.figure()
        x = self.simulations[idx].x[0, 0, :]
        y = self.simulations[idx].x[1, 0, :]
        plt.subplot(2, 1, 1)
        plt.plot(x, y)

        # limit to square output
        buf = 0.5  # scaled distance around maximum point
        max_x_val = max(x)
        max_y_val = max(y)
        min_x_val = min(x)
        min_y_val = min(y)

        x_range = max_x_val - min_x_val
        y_range = max_y_val - min_y_val
        square_len = max(x_range, y_range)
        x_mid = (max_x_val + min_x_val) / 2
        y_mid = (max_y_val + min_y_val) / 2
        plt.xlim(x_mid - (square_len / 2 + square_len * buf),
                 x_mid + (square_len / 2 + square_len * buf))
        plt.ylim(y_mid - (square_len / 2 + square_len * buf),
                 y_mid + (square_len / 2 + square_len * buf))

        plt.xlabel("$x$ posistion")
        plt.ylabel("$y$ posistion")

        plt.subplot(2, 1, 2)
        x = self.simulations[idx].x_hat[0, 0, :]
        y = self.simulations[idx].x_hat[1, 0, :]
        plt.plot(x, y)
        plt.xlim(x_mid - (square_len / 2 + square_len * buf),
                 x_mid + (square_len / 2 + square_len * buf))
        plt.ylim(y_mid - (square_len / 2 + square_len * buf),
                 y_mid + (square_len / 2 + square_len * buf))

        plt.xlabel("$\hat{x}$ posistion")
        plt.ylabel("$\hat{y}$ posistion")

    def plot_trajectory_together(self, trajectory_num, points=160):
        plt.plot(ut.downsample_array(self.simulations[0].x[0, 0], points=points),
                 ut.downsample_array(self.simulations[0].x[1, 0], points=points),
                 label="$\mathbf{x}$")

        plt.plot(ut.downsample_array(self.simulations[0].x_hat[0, 0], points=points),
                 ut.downsample_array(self.simulations[0].x_hat[1, 0], points=points),
                 label="$\hat{\mathbf{x}}$")

        plt.axis('equal')

        plt.xlabel("Posistion $x$ (m)")
        plt.ylabel("Posistion $y$ (m)")

    def print_update(self, idx):
        print("%6.2f%% - Time: %.2f sec" %
              ((idx + 1) / float(self.num_simulations) * 100,
               time.time() - self.start))

    def end_routine(self):
        print("")
        print("Monte Carlo Simulations: %6d sims" % self.num_simulations)
        print("  Length of simulations: %6.2f secs" % self.simulation_len)
        print(" Total computation time: %6.2f secs" % (time.time() - self.start))
        self.calc_error()
        self.calc_three_sig()
        self.calc_residual_mean()

    def calc_error(self):
        self.error = zeros((self.num_simulations, 5, len(self.time)))
        for idx, simulation in enumerate(self.simulations):
            self.error[idx, 0, :] = simulation.x_hat[0, 0, :] - simulation.x[0, 0, :]
            self.error[idx, 1, :] = simulation.x_hat[1, 0, :] - simulation.x[1, 0, :]
            self.error[idx, 2, :] = simulation.x_hat[2, 0, :] - simulation.x[2, 0, :]
            self.error[idx, 3, :] = simulation.x_hat[3, 0, :] - simulation.x[5, 0, :]
            self.error[idx, 4, :] = simulation.x_hat[4, 0, :] - simulation.x[6, 0, :]
        return self.error

    def calc_residual_mean(self):
        self.residual_mean = zeros((self.num_simulations, 2, len(self.simulations[0].resid_t)))
        for idx, simulation in enumerate(self.simulations):
            self.residual_mean[idx, 0, :] = simulation.resid[0, :]
            self.residual_mean[idx, 1, :] = simulation.resid[1, :]
        self.residual_mean = np.mean(self.residual_mean, axis=0)
        return self.residual_mean

    def calc_three_sig(self):
        self.error_three_sig = zeros((5, len(self.time)))
        for idx in range(5):
            self.error_three_sig[idx, :] = np.std(self.error[:, idx, :], axis=0) * 3

    def plot_state(self, idx, state):
        plt.plot(self.time, self.simulations[idx].x[state, 0, :])

    def plot_state_estimate(self, idx, state):
        plt.plot(self.time, self.simulations[idx].x_hat[state, 0, :])

    def plot_state_error(self, state, points=160):
        for error in self.error[:, state, :]:
            plt.plot(ut.downsample_array(self.time, points=points),
                     ut.downsample_array(error, points=points),
                     linewidth=.5)

    def plot_P(self, state, points=160):
        plt.plot(ut.downsample_array(self.time, points=points),
                 ut.downsample_array(np.sqrt(self.simulations[-1].P[state, state]) * 3, points=points),
                 linewidth=0.75, color="red", linestyle="--",
                 label="Monte Carlo $3\sigma$")

        plt.plot(ut.downsample_array(self.time, points=points),
                 ut.downsample_array(-np.sqrt(self.simulations[-1].P[state, state]) * 3, points=points),
                 linewidth=0.75, color="red", linestyle="--",
                 label="Monte Carlo $3\sigma$")

        # plt.legend(["Kalman Filter $3\sigma$"], loc='upper left', )

    def plot_state_error_three_sig(self, state, points=160):
        alpha = 0.2
        plt.fill_between(ut.downsample_array(self.time, points=points),
                         ut.downsample_array(self.error_three_sig[state], points=points),
                         ut.downsample_array(self.error_three_sig[state], points=points) * -1,
                         facecolor='red', alpha=alpha, step="pre",
                         label="Kalman Filter $3\sigma$")

    def plot_state_monte_carlo(self, state, points=160):
        self.plot_state_error_three_sig(state, points=points)
        self.plot_state_error(state, points=points)
        self.plot_P(state, points=points)
        plt.ylabel(self.y_labels[state])

    def subplot_monte_carlo(self, points=500):
        num_states = 5
        for idx in range(num_states):
            plt.figure(figsize=(8, 4))
            # plt.subplot(num_states, 1, idx + 1)
            self.plot_state_monte_carlo(idx, points=points)
            plt.grid(linestyle='-', linewidth=.25)
            plt.legend()
            plt.xlabel("Time (sec)")

        plt.figure()
        self.show_residual()

        # plt.figure()
        # plt.subplot(2, 1, 1)
        # plt.step(self.simulations[0].resid_t, self.residual_mean[0])
        # plt.ylabel("x Residual Mean (m)")
        #
        # plt.subplot(2, 1, 2)
        # plt.step(self.simulations[1].resid_t, self.residual_mean[1])
        # plt.ylabel("y Residual Mean (m)")
        # plt.xlabel("Time (s)")

    def plot_residual_state(self, state, points=160):
        plt.plot(ut.downsample_array(self.time, points=points),
                 ut.downsample_array(np.sqrt(self.simulations[state].resid_P[state, state]) * 3, points=points),
                 linewidth=0.9, color="red", linestyle="--", label="Kalman Filter Residual $3\sigma$")
        plt.plot(ut.downsample_array(self.time, points=points),
                 ut.downsample_array(-np.sqrt(self.simulations[state].resid_P[state, state]) * 3, points=points),
                 linewidth=0.9, color="red", linestyle="--")
        plt.xlabel("Time (s)")

        for simulation in self.simulations:
            plt.scatter(simulation.resid_t, simulation.resid[state, :])

    def show_residual(self, points=160):
        plt.subplot(2, 1, 1)
        self.plot_residual_state(0, points=points)
        plt.ylabel("$x$ Residual (m)")
        plt.legend()

        plt.subplot(2, 1, 2)
        self.plot_residual_state(1, points=points)
        plt.ylabel("$y$ Residual (m)")
        plt.legend()
        plt.xlabel("Time (s)")

    def save_figure_pdf(self, file_name):
        plt.savefig(file_name, format='pdf')

    def save_all_figures_pdf(self):
        # pdf save directory
        pdf_trajectory = PdfPages("./pdf/trajectory.pdf")
        pdf_state_error = PdfPages("./pdf/errors.pdf")
        pdf_residuals = PdfPages("./pdf/resisuals.pdf")

        # save state error plots
        font = {'family': 'normal',
                'weight': 'bold',
                'size': 12}
        plt.rc('font', **font)

        # save individual state error plots
        plt.figure(num=None, figsize=(7.5, 4), dpi=100)
        for idx in range(5):
            self.plot_state_monte_carlo(idx)
            plt.grid(linestyle='-', linewidth=.25)
            plt.legend(loc='upper center', bbox_to_anchor=(0.5, 1.1),
                       ncol=2, fancybox=True, shadow=True)
            self.save_figure_pdf(pdf_state_error)
            plt.clf()

        # save subplots for first three states
        plt.figure(num=None, figsize=(7.5, 8), dpi=100)
        self.subplot_monte_carlo()
        self.save_figure_pdf(pdf_state_error)
        plt.clf()

        # save true trajectory plot
        plt.figure(num=None, figsize=(6, 4.5),
                   dpi=100, facecolor='w', edgecolor='k')
        self.plot_trajectory(0)
        self.save_figure_pdf(pdf_trajectory)

        # save residual plots

        pdf_trajectory.close()
        pdf_state_error.close()
        pdf_residuals.close()

    def calc_y_error(self, variation, monte, controller):
        y_error = np.zeros((variation.y_error_three_sig.shape[-1], monte.num_simulations))
        for i in range(monte.num_simulations):
            x_pos = monte.simulations[i].x[0, :, variation.analysis_start_idx:].squeeze()
            y_pos = monte.simulations[i].x[1, :, variation.analysis_start_idx:].squeeze()
            y_error[:, i] = self.calc_y_error_point(x_pos, y_pos, controller.trajectory)
        return y_error

    def calc_y_error_point(self, x, y, trajectory):
        x_y_pos = np.c_[x, y].T
        # calculate unit vector heading for each point on trajectory
        R = ut.rotation_mat(trajectory.theta)
        x_y_traj = np.c_[trajectory.x, trajectory.y].T
        v_r = np.zeros((2, x_y_traj.shape[-1]))
        unit = np.array([[1], [0]])
        for idx in range(R.shape[-1]):
            v_r[:, [idx]] = R[:, :, idx].dot(unit)

        # calculate y_error from unit vector heading
        # normalize error vector
        v_e_len = x_y_pos.shape[-1]
        v_e = x_y_pos - x_y_traj[:, 0:v_e_len]
        R_90 = ut.rotation_mat(trajectory.theta + np.pi / 2.0)

        y_error = np.zeros(v_e.shape[-1])
        for idx in range(y_error.shape[-1]):
            y_error[idx] = v_e[:, idx].T.dot(R_90[:, :, idx]).dot(v_r[:, idx])

        return y_error

    def calc_charge_profile(self, monte):
        # compute estimated charge given delta
        charge = np.zeros(monte.num_simulations)
        for i in range(monte.num_simulations):
            charge[i] = np.average(monte.simulations[i].watt_hour_charging)
        charge_mu = np.average(charge)
        charge_std = np.std(charge)
        return charge_mu, charge_std

    def verify_charge_percent_below_q_d(self, monte, params):
        fig, ax = plt.subplots()
        charge = np.zeros(monte.num_simulations)
        charge_lt_desired = 0
        for i in range(monte.num_simulations):
            charge[i] = np.average(monte.simulations[i].watt_hour_charging)
            if charge[i] < params.desired_charge:
                charge_lt_desired = charge_lt_desired + 1

        percent_success = 1 - charge_lt_desired / float(monte.num_simulations)
        charge_mu = np.average(charge)
        charge_std = np.std(charge)

        ax.plot(charge, "o")
        ax.set_xlabel("Simulation Number")
        ax.set_ylabel("Expected Charge")

        print("Average Charge: %.3f" % charge_mu)
        print("Standard Deviation of Average Charge: %.3f" % charge_std)
        print("Percent Succesfull Charge: %.3f" % percent_success)

        return percent_success

    def verify_delta_max(self, y_error, params):
        fig, ax = plt.subplots()
        max_errors = np.max(abs(y_error), axis=0)
        num_outside_road = 0
        for max_error in max_errors:
            if max_error < np.sqrt(params.sig2_delta) * 3:
                num_outside_road = num_outside_road + 1

        ax.plot(max_errors, "o")
        print("Numer of simulations outside of desired charge: %d" % num_outside_road)
        ax.set_xlabel("Simulation Number")
        ax.set_ylabel("Maximum Absolute Value of \delta")

    def plot_three_sigma(self, x, three_sig):
        plt.plot(x, three_sig,
                 linewidth=0.75, color="red", linestyle="--",
                 label="Monte Carlo $3\sigma$")
        plt.plot(x, -three_sig,
                 linewidth=0.75, color="red", linestyle="--",
                 label="Monte Carlo $3\sigma$")

    def plot_three_sigma_avg(self, x, three_sig_avg):
        plt.plot(x, three_sig_avg * np.ones(x.shape),
                 linewidth=0.75, color="k", linestyle="--",
                 label="Average Monte Carlo $3\sigma$")
        plt.plot(x, -three_sig_avg * np.ones(x.shape),
                 linewidth=0.75, color="k", linestyle="--",
                 label="Average Monte Carlo $3\sigma$")

    def plot_sim_trajectory(self, monte, start, stop):
        # plot vehicle trajectory for each simulation
        for i in range(monte.num_simulations):
            x_pos = monte.simulations[i].x[0, :, start:stop].squeeze()
            y_pos = monte.simulations[i].x[1, :, start:stop].squeeze()
            plt.plot(x_pos, y_pos)

    def plot_trajectories(self, monte, start, stop, k_val):
        fig, ax = plt.subplots()
        for i in range(monte.simulations[0].num_simulations):
            x_pos = monte.simulations[i].x[0, :, start:stop].squeeze()
            y_pos = monte.simulations[i].x[1, :, start:stop].squeeze()
            ax.plot(x_pos, y_pos)
        ax.set_title("K=%.4f" % k_val)
        ax.set_xlabel("x position (m)")
        ax.set_ylabel("y position (m)")

    def plot_watt_charging(self, monte, k_val):
        fig, ax = plt.subplots()
        for i in range(monte.simulations[0].num_simulations):
            ax.plot(monte.simulations[i].time, monte.simulations[i].watt_hour_charging)
        ax.set_title("K=%.4f" % k_val)
        ax.set_xlabel("Time (sec)")
        ax.set_ylabel("Charge (Watts)")


class MonteSim:
    def __init__(self, params):
        # simulation parameters
        self.num_simulations = params.simulations
        self.end_time = params.end_time  # seconds
        self.dt = params.dt  # integration time step
        self.time = np.arange(0, self.end_time, self.dt)

        # vehicle arrays
        self.x_dim = [7, 1]
        self.x = zeros((self.x_dim[0], self.x_dim[1], self.time.shape[-1]))
        self.x_hat_dim = [5, 1]
        self.x_hat = zeros((self.x_hat_dim[0], self.x_hat_dim[1], self.time.shape[-1]))
        self.u = zeros((2, self.time.shape[-1]))

        # kalman filter arrays
        self.num_measurements = 2
        self.K_dim = [self.x_hat_dim[0], self.num_measurements]
        self.P = zeros((self.x_hat_dim[0], self.x_hat_dim[0], self.time.shape[-1]))
        self.resid = np.array([[], []])
        self.resid_t = np.array([])
        self.resid_P = zeros((2, 2, self.time.shape[-1]))
        self.K = zeros((self.x_hat_dim[0], self.num_measurements, self.time.shape[-1]))

        # controller arrays
        self.y_eps = zeros((2, self.x_dim[1], self.time.shape[-1]))
        self.y_eps_hat = zeros((2, self.x_dim[1], self.time.shape[-1]))

        # charging arrays
        self.watt_hour_motor_usage = zeros((self.time.shape[-1]))
        self.watt_hour_charging = zeros((self.time.shape[-1]))
        self.state_of_charge = zeros((self.time.shape[-1]))


if __name__ == '__main__':
    main()
