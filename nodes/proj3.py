from Parameters import Parameters
from VehicleSim import VehicleSim
import numpy as np
from utils import Trajectory2D
from ControllerYEpsilon import ControllerYEpsilon
import matplotlib.pyplot as plt


def main():
    params = Parameters()
    controller = ControllerYEpsilon(params)
    sim = VehicleSim(params)

    time = np.arange(0, 20, params.dt)
    x_output = np.zeros((7, 1, len(time)))
    u_output = np.zeros((2, 1, len(time)))

    controller.eps = 2
    sim.x = x_output[:, :, 0]

    for t in range(len(time)):
        # calculate controller input
        u = controller.calc_input_yd(sim.x, [20, 20], [0, 0])

        # propagate vehicle forward
        sim.propagate(u)
        x_output[:, :, t] = sim.x
        u_output[:, :, t] = u

    fig1, ax1 = plt.subplots()
    ax1.plot(x_output[0, :, :].squeeze(), x_output[1, :, :].squeeze())
    plt.show()


if __name__ == '__main__':
    main()
