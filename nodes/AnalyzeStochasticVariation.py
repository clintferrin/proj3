#! /usr/bin/env python
# coding=utf-8

import Parameters as pm
from StochasticVariation import StochasticVariation
import matplotlib.pyplot as plt
import numpy as np
import scipy.stats


def main():
    params = pm.Parameters()
    variation = StochasticVariation(1, 1, 1, params)
    variation = variation.load("simulations/w_l=w_h-sim=250-K=0.1:0.975-num=36.stoch")

    variation.plot_charge_avg()
    _, _, charge_mu = plot_fit(variation.k_range, variation.charge_mu, 2, plot=True)
    # _, _, charge_mu = plot_fit(variation.k_range, variation.charge_mu, 3, plot=True)

    variation.plot_charge_3_sig()
    _, _, charge_sig = plot_fit(variation.k_range, variation.charge_std, 2, plot=True)
    # _, _, charge_sig = plot_fit(variation.k_range, variation.charge_std, 4, plot=True)

    # variation.plot_y_error_3_sig_avg()
    # plot_fit(variation.k_range, variation.y_error_three_sig_avg, 2)

    desired_charge = 40000
    # z = 1
    confidence_interval = .97
    z = scipy.stats.norm.ppf(confidence_interval)


    # z = scipy.stats.norm.ppf((1 + confidence_interval) / 2.0)
    plot_mean_sig(variation.k_range, charge_mu, charge_sig, z)

    # new_system = np.array(charge_mu - z * charge_sig)
    # new_system = np.append(new_system[:-1], new_system[-1] - desired_charge)
    #
    # roots = np.roots(new_system)
    # K = roots[-1]

    alpha = (charge_mu[1] - z * charge_sig[1]) / (charge_mu[2] - z * charge_sig[2])
    beta = (desired_charge - (charge_mu[0] - z * charge_sig[0])) / (charge_mu[2] - z * charge_sig[2])
    K = -np.sqrt(beta + 1 / 4.0 * alpha ** 2) - 1 / 2.0 * alpha
    print("K: %.2f" % K)
    plt.scatter(np.real(K), desired_charge)

    plt.show()


def plot_fit(x, y, order, num_pts=50, plot=True):
    poly_fit = np.polyfit(x, y, order)
    poly_fit = np.poly1d(poly_fit)
    x = np.linspace(x[0], x[-1], num_pts)
    y = poly_fit(x)
    if plot is True:
        plt.plot(x, y)
    return x, y, poly_fit


def plot_mean_sig(k, poly_mean, poly_sig, z):
    x = np.linspace(k[0], k[-1], 100)
    fig, ax = plt.subplots()

    # y = np.clip(poly_mean(x), 0, 60000)
    y = poly_mean(x)
    # y_sig = np.clip(z * poly_sig(x), 0, np.inf)
    y_sig = z * poly_sig(x)

    ax.plot(x, y)
    # y_out = poly_mean(x) - poly_sig(x)

    # calculate sigma value
    ax.plot(x, y + y_sig)
    ax.plot(x, y - y_sig)

    ax.set_xlabel("K (Magnitude)")
    ax.set_ylabel("Charge Avg (Watts)")


if __name__ == '__main__':
    main()
