#! /usr/bin/env python
# coding=utf-8

import numpy as np
from numpy import diag, sqrt, pi
from numpy.random import normal
import VehicleSim as vs
import Parameters
from utils import rotation_mat


def main():
    params = Parameters.Parameters()
    # sim = vs.VehicleSim(params)
    # x = sim.propagate([0, 0])
    # sensors = SensorSuite(params)
    # phi = sensors.get_steering_angle(x)
    # gps = sensors.get_gps_pos(x)
    # print(gps)


class SensorSuite:
    def __init__(self, params):
        # obtain necessary function information from params
        self.dt = params.dt
        self.gps_freq = params.gps_freq

        # variable sizing parameters
        self.v_sig = params.v_sig  # sd of vel measurement [m] after v_sig_time_des
        self.phi_sig = params.phi_sig  # sd of angle measurement [rad] after phi_sig_time_des
        self.v_sig_time_des = params.v_sig_time_des
        self.phi_sig_time_des = params.phi_sig_time_des
        self.v_nominal = params.v_nominal  # nominal speed for sizing Q_phi
        self.L = params.wheel_base
        self.gps_ref = params.gps_distance_from_back_axel

        # size Q for sensor noise b/c not given in sensor suite
        q_v = self.v_sig ** 2 / float(self.v_sig_time_des) ** 2
        q_phi = self.phi_sig ** 2 / float(self.phi_sig_time_des) ** 2 \
                * (self.L ** 2) / float(self.v_nominal) ** 2
        q_v_bias = 2 * params.v_b_sig_ss ** 2 / float(params.tau_v)
        q_phi_bias = 2 * params.phi_b_sig_ss ** 2 / float(params.tau_phi)

        # build Q matrix
        self.Q = diag([q_v,
                       q_phi,
                       q_v_bias,
                       q_phi_bias])

        # build R matrix for discrete sensors
        self.gps_x_sig = params.gps_x_sig
        self.gps_y_sig = params.gps_y_sig

        self.R = diag([self.gps_x_sig ** 2,
                       self.gps_y_sig ** 2])

        # calculate noise multiplication factors
        self.v_noise_scale = sqrt(self.Q[0, 0] / self.dt)
        self.phi_noise_scale = sqrt(self.Q[1, 1] / self.dt)

        # turn off noise
        if not params.noise:
            self.v_noise_scale = 0
            self.phi_noise_scale = 0
            self.gps_x_sig = 0
            self.gps_y_sig = 0

    def get_velocity(self, x):
        return x[3, 0] + x[5, 0] + normal() * self.v_noise_scale

    def get_steering_angle(self, x):
        return x[4, 0] + x[6, 0] + normal() * self.phi_noise_scale

    def get_gps_pos(self, x):
        # rotate gps location into reference frame
        gps_ref = rotation_mat(x[2, 0]).dot(np.array([[self.gps_ref], [0]]))
        gps_pos = x[0:2] + gps_ref + np.array([[normal() * self.gps_x_sig],
                                               [normal() * self.gps_y_sig]])
        return gps_pos


if __name__ == '__main__':
    main()
