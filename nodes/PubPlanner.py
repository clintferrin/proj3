#! /usr/bin/env python

import Parameters
import SensorSuite as ss
import rospy
from numpy import array, arange
from vehicle_sim.msg import path_trajectory, bicycle_state, velocity_trajectory
from PathPlanner import PathPlanner


def main():
    params = Parameters.Parameters()
    path_planner = PathPlanner(params)
    pub_planner = PubPlanner(path_planner, params)
    pub_planner.publish(hz=params.pub_planner_freq)


class PubPlanner:
    def __init__(self, path_planner, params):
        self.planner = path_planner
        self.params = params
        self.x_hat = array([[0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0]])
        rospy.init_node('path_planner', anonymous=True)
        rospy.Subscriber("/x_state", bicycle_state, self.__callback)

    def __callback(self, data):
        self.x_hat[0, 0] = data.x
        self.x_hat[1, 0] = data.y
        self.x_hat[2, 0] = data.psi
        self.x_hat[3, 0] = data.v
        self.x_hat[4, 0] = data.phi
        self.x_hat[5, 0] = data.b_v
        self.x_hat[6, 0] = data.b_phi

    def publish(self, hz=100):
        pub_velocity = rospy.Publisher('velocity_plan', velocity_trajectory, queue_size=10)
        pub_path = rospy.Publisher('position_plan', path_trajectory, queue_size=10)

        rate = rospy.Rate(hz)  # hz
        v_msg = velocity_trajectory()
        p_msg = path_trajectory()

        while not rospy.is_shutdown():
            x = self.x_hat.copy()
            new_plan = self.planner.update_controller_trajectory(x)

            if new_plan is True:
                v_msg.header.stamp = rospy.Time.now()
                p_msg.header.stamp = rospy.Time.now()

                v_msg.trajectory = self.planner.get_velocity_trajectory().path
                v_msg.trajectory_dot = self.planner.get_velocity_trajectory().path_dot

                p_msg.x = self.planner.get_position_trajectory().x
                p_msg.y = self.planner.get_position_trajectory().y

                pub_velocity.publish(v_msg)
                pub_path.publish(p_msg)

            rate.sleep()


if __name__ == '__main__':
    main()
