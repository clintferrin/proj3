#! /usr/bin/env python
# coding=utf-8

from numpy import eye, array, c_, r_, array, diag, cos, sin, tan
import Parameters as pm
import VehicleSim as ve
import numpy as np
from utils import rk4, Trajectory2D, Trajectory1D


def main():
    # initialization
    params = pm.Parameters()
    sim = ve.VehicleSim(params)
    v_controller = VeclocityController(params)
    u = v_controller.calc_input(10)
    print(u)


class OldVeclocityController:
    def __init__(self, params):
        self.t = 0
        self.dt = params.dt
        self.trajectory = Trajectory2D
        self.vd = array([[0.0], [0.0], [0.0], [0.0]])
        self.moving_average = np.zeros(1)
        self.moving_average_idx = int(0)
        self.error_sign = -1

        A = 0
        B = 1
        C = 1

        # set up double integrator
        Abar = array([[A, 0, 0, 0],
                      [C, 0, 0, 0],
                      [0, 1, 0, 0],
                      [0, 0, 1, 0]])

        Bbar = array([[B], [0], [0], [0]])

        # initialize cost variables
        Q = diag([30000, 1, 1, 1])
        R = 10000
        self.K = np.array([1.1844, 0.6515, 0.2054, 0.0316])
        self.x_bar = array([[float(params.x0[3])], [0.0], [0.0], [0.0]])

    def build_trajectory(self):
        self.t = 0
        end_time = 10.0
        end_speed = 26.8224
        dt = self.dt

        m = end_speed / end_time
        x = np.arange(0, end_time + dt, dt)
        self.set_trajectory(self.__linear_function(x, m, 0))

    def __build_stopping_trajectory(self, v):
        start_speed = v
        end_speed = 0
        m = -8.0  # stops quickly
        end_time = (end_speed - start_speed) / m
        dt = self.dt
        x = np.arange(start_speed, end_time + dt, dt)
        self.trajectory = self.__linear_function(x, m, start_speed)

    def set_trajectory(self, trajectory):
        self.trajectory = trajectory
        self.t = 0

    def __linear_function(self, x, m, b):
        return x * m + b

    def calc_input(self, v):
        v = self.calc_moving_average(v)
        if len(self.trajectory.velocity) == 0:
            self.__build_stopping_trajectory(v)

        elif self.t == len(self.trajectory.velocity) - 1:
            self.vd[0] = self.trajectory.velocity[self.t]
            self.x_bar[1:, :] = self.x_bar[1:, :] * .2
            self.t = self.t + 1

        elif self.t > len(self.trajectory.velocity) - 1:
            self.vd[0] = self.trajectory.velocity[-1]

        else:
            self.vd[0] = self.trajectory.velocity[self.t]
            self.t = self.t + 1

        self.x_bar[0] = v
        error = v - self.vd[0]

        u = -self.K.dot(self.x_bar - self.vd)[0]
        self.x_bar = rk4(self.__prop_x_bar, self.dt, self.x_bar, u, error)
        return u

    def __prop_x_bar(self, x_bar, u, error):
        return np.array([[u],
                         [error],
                         [0],
                         [0]])

    def calc_moving_average(self, v):
        self.moving_average[self.moving_average_idx % len(self.moving_average)] = v
        self.moving_average_idx = self.moving_average_idx + 1
        return np.mean(self.moving_average)

    def calc_input_simple(self, v):
        v = self.calc_moving_average(v)
        if self.t == len(self.trajectory.velocity) - 1:
            self.vd[0] = self.trajectory.velocity[self.t]
            self.x_bar[1:, :] = self.x_bar[1:, :] * .2
            self.t = self.t + 1

        elif self.t > len(self.trajectory.velocity) - 1:
            self.vd[0] = self.trajectory.velocity[-1]
            self.x_bar[1:, :] = self.x_bar[1:, :] * 0

        else:
            self.vd[0] = self.trajectory.velocity[self.t]
            self.t = self.t + 1

        return -(v - self.vd[0])


class VeclocityController:
    def __init__(self, params):
        self.t = 0
        self.direction = 1
        self.dt = params.dt
        self.trajectory = Trajectory1D(ds=self.dt)
        self.vd = array([[0.0], [0.0], [0.0], [0.0]])
        self.moving_average = np.zeros(1)
        self.moving_average_idx = int(0)
        self.error_sign = -1

        # initalize vehicle dynamics
        rho = params.air_density
        A = params.front_area
        Cd = params.drag_coefficient
        fm = params.mass_factor
        self.M = params.mass
        self.A = -rho * A * Cd / float(fm * self.M)
        self.B = 1 / float(fm * self.M)

        # initialize PID values
        self.integral = 0
        self.error_old = 0
        self.derivative = None

        self.Kp = 300
        self.Ki = 90
        self.Kd = 0

    def set_trajectory(self, trajectory):
        self.trajectory = trajectory
        self.t = 0

    def __linear_function(self, x, m, b):
        return x * m + b

    def __build_stopping_trajectory(self, v):
        start_speed = v
        end_speed = 0
        m = -8.0  # stops quickly
        end_time = (end_speed - start_speed) / m
        dt = self.dt
        x = np.arange(start_speed, end_time + dt, dt)
        self.trajectory.path = self.__linear_function(x, m, start_speed)
        self.trajectory.path_dot = np.zeros(x.shape) + m
        self.t = 0

    def calc_input(self, v):
        v = self.calc_moving_average(v)

        if len(self.trajectory.path) == 0:
            self.__build_stopping_trajectory(v)
            vd = self.trajectory.path[self.t]
            vd_dot = self.trajectory.path_dot[self.t]
            self.t = self.t + 1

        elif self.t == len(self.trajectory.path) - 1:
            vd = self.trajectory.path[self.t]
            vd_dot = self.trajectory.path_dot[self.t] * .1
            self.t = self.t + 1

        elif self.t > len(self.trajectory.path) - 1:
            vd = self.trajectory.path[-1]
            vd_dot = self.trajectory.path_dot[-1] * .1

        else:
            vd = self.trajectory.path[self.t]
            vd_dot = self.trajectory.path_dot[-1]
            self.t = self.t + 1

        uff = (vd_dot - self.A * vd) / float(self.B)
        error = v - vd

        self.integral = self.integral + error * self.dt
        derivative = (error - self.error_old) / self.dt

        p_term = self.Kp * error
        i_term = self.Ki * self.integral
        d_term = self.Kd * derivative

        u = uff - (p_term + i_term + d_term)

        self.error_old = error
        if u < 0 and self.direction > 0:
            return 0

        else:
            return u / float(self.M)

    def __prop_x_bar(self, x_bar, u, error):
        return np.array([[u],
                         [error],
                         [0],
                         [0]])

    def calc_moving_average(self, v):
        self.moving_average[self.moving_average_idx % len(self.moving_average)] = v
        self.moving_average_idx = self.moving_average_idx + 1
        return np.mean(self.moving_average)


if __name__ == '__main__':
    main()
