#! /usr/bin/env python
# coding=utf-8

import Parameters as pm
import numpy as np
import pickle
import matplotlib.pyplot as plt
import time


def main():
    params = pm.Parameters()
    variation = StochasticVariation(0, .4, .001, params)


class StochasticVariation:
    def __init__(self, k_start, k_end, k_dt, params):
        # simulation attributes
        self.k_range = np.arange(k_start, k_end, k_dt)
        self.k_len = len(self.k_range)
        self.analysis_start_time = params.analysis_start_time
        self.analysis_start_idx = int(round(self.analysis_start_time / params.dt))
        self.analysis_idx_len = int(np.round((params.end_time - params.analysis_start_time) / params.dt))

        # analysis arrays
        self.y_error_three_sig_avg = np.zeros(self.k_len)
        self.charge_mu = np.zeros(self.k_len)
        self.charge_std = np.zeros(self.k_len)
        self.start = time.time()
        self.y_error_three_sig = np.zeros((self.k_len, self.analysis_idx_len))

        # print update parameters
        self.num_simulations = params.simulations
        self.simulations = []

    def plot_charge_avg(self):
        fig, ax = plt.subplots()
        ax.plot(self.k_range, self.charge_mu)
        ax.set_xlabel("K (magnitude)")
        ax.set_ylabel("Charge Avg (Watts)")

    def plot_charge_3_sig(self):
        fig, ax = plt.subplots()
        ax.plot(self.k_range, self.charge_std)
        ax.set_xlabel("K (magnitude)")
        ax.set_ylabel("Charge $\sigma$ (Watts)")

    def plot_y_error_3_sig_avg(self):
        fig, ax = plt.subplots()
        ax.plot(self.k_range, self.y_error_three_sig_avg)
        ax.set_xlabel("K (magnitude)")
        ax.set_ylabel("y error 3 $\sigma$ (m)")

    def save(self, file_path):
        pickle.dump(self, open(file_path + ".stoch", "wb"))

    def load(self, load_file):
        return pickle.load(open(load_file, "rb"))

    def print_update(self, k_num, sim_num):
        complete = ((k_num * self.num_simulations) + sim_num + 1) / float(self.k_len * self.num_simulations)
        print("\nTotal Percent Complete: %6.2f%%" % (100 * complete))
        elapsed_time = time.time() - self.start
        estimated_total_time = elapsed_time / float(complete)
        estimated_time_remaining = estimated_total_time - estimated_total_time * complete
        hours, minutes, seconds = self.timer(0, estimated_time_remaining)

        if hours < 1 and minutes < 1:
            print("Estimated Time Remaining: %.2f sec" % seconds)
        elif hours < 1:
            print("Estimated Time Remaining: %d min %.2f sec" % (minutes, seconds))
        else:
            print("Estimated Time Remaining: %d hr %d min %.2f sec" % (hours, minutes, seconds))

    def timer(self, start, end):
        hours, rem = divmod(end - start, 3600)
        minutes, seconds = divmod(rem, 60)
        return hours, minutes, seconds


if __name__ == '__main__':
    main()
