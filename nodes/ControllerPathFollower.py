#! /usr/bin/env python
# coding=utf-8

from numpy import eye, array, c_, r_, array, diag, cos, sin, tan
import Parameters as pm
import VehicleSim as ve
import numpy as np
from utils import rk4, Trajectory2D, Trajectory1D


def main():
    # initialization
    params = pm.Parameters()
    sim = ve.VehicleSim(params)
    v_controller = PathFollower(params)
    u = v_controller.calc_input(10)
    print(u)

class PathFollower:
    def __init__(self, params):
        self.y_eps = params.control_reference
        self.Kd = 0

    def set_trajectory(self, trajectory):
        self.trajectory = trajectory
        self.t = 0

    def __linear_function(self, x, m, b):
        return x * m + b

    def __build_stopping_trajectory(self, v):
        start_speed = v
        end_speed = 0
        m = -8.0  # stops quickly
        end_time = (end_speed - start_speed) / m
        dt = self.dt
        x = np.arange(start_speed, end_time + dt, dt)
        self.trajectory.path = self.__linear_function(x, m, start_speed)
        self.trajectory.path_dot = np.zeros(x.shape) + m
        self.t = 0

    def calc_input(self, v):
        v = self.calc_moving_average(v)

        if len(self.trajectory.path) == 0:
            self.__build_stopping_trajectory(v)
            vd = self.trajectory.path[self.t]
            vd_dot = self.trajectory.path_dot[self.t]
            self.t = self.t + 1

        elif self.t == len(self.trajectory.path) - 1:
            vd = self.trajectory.path[self.t]
            vd_dot = self.trajectory.path_dot[self.t] * .1
            self.t = self.t + 1

        elif self.t > len(self.trajectory.path) - 1:
            vd = self.trajectory.path[-1]
            vd_dot = self.trajectory.path_dot[-1] * .1

        else:
            vd = self.trajectory.path[self.t]
            vd_dot = self.trajectory.path_dot[-1]
            self.t = self.t + 1

        uff = (vd_dot - self.A * vd) / float(self.B)
        error = v - vd

        self.integral = self.integral + error * self.dt
        derivative = (error - self.error_old) / self.dt

        p_term = self.Kp * error
        i_term = self.Ki * self.integral
        d_term = self.Kd * derivative

        u = uff - (p_term + i_term + d_term)

        self.error_old = error
        if u < 0 and self.direction > 0:
            return 0

        else:
            return u / float(self.M)

    def __prop_x_bar(self, x_bar, u, error):
        return np.array([[u],
                         [error],
                         [0],
                         [0]])

    def calc_moving_average(self, v):
        self.moving_average[self.moving_average_idx % len(self.moving_average)] = v
        self.moving_average_idx = self.moving_average_idx + 1
        return np.mean(self.moving_average)


if __name__ == '__main__':
    main()
