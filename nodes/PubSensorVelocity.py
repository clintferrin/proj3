#! /usr/bin/env python

import Parameters
import SensorSuite as ss
import rospy
from numpy import array
from vehicle_sim.msg import bicycle_state
import std_msgs.msg as std_msgs


def main():
    params = Parameters.Parameters()
    sensors = ss.SensorSuite(params)
    velocity_measurement = PubSensorVelocity(sensors)
    velocity_measurement.publish(hz=params.velocity_sensor_freq)
    # phi = sensors.get_steering_angle(x)

    # spin() simply keeps python from exiting until this node is stopped


class PubSensorVelocity:
    def __init__(self, sensors):
        self.sensors = sensors
        self.phi = 0
        self.b_phi = 0
        self.x = array([[0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0]])
        rospy.init_node('v_tilde', anonymous=True)
        rospy.Subscriber("x_state", bicycle_state, self.__callback)

    def __callback(self, data):
        self.x[3, 0] = data.v
        self.x[5, 0] = data.b_v

    def publish(self, hz=100):
        pub = rospy.Publisher('v_tilde', std_msgs.Float64, queue_size=10)
        rate = rospy.Rate(hz)  # hz
        while not rospy.is_shutdown():
            v_tilde = self.sensors.get_velocity(self.x).copy()
            log_str = "v_tilde: " + str(v_tilde)
            rospy.loginfo(log_str)
            pub.publish(v_tilde)
            rate.sleep()


if __name__ == '__main__':
    main()
