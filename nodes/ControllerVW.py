#! /usr/bin/env python
# coding=utf-8

from numpy import eye, array, c_, r_, array, diag, cos, sin, tan
from Parameters import Parameters
from VehicleSim import VehicleSim
import matplotlib.pyplot as plt
import numpy as np
import scipy.linalg
from utils import Trajectory2D
from ControlVariation import ControlVariation


def main():
    # initialization
    params = Parameters()
    controller = ControllerVW(params)


class ControllerVW:
    def __init__(self, params):
        # vehicle parameters
        self.params = params
        self.L = params.wheel_base  # meters
        self.max_steer_angle = params.max_steer_angle  # phi
        self.max_change_steer_angle = params.max_change_steer_angle  # per second
        self.max_velocity_limit = params.max_velocity_limit  # m/s
        self.max_accel_limit = params.max_accel_limit  # m/s^2

        # control parameters
        self.t = 0
        self.dt = params.dt
        self.eps = params.control_reference  # m from back axel reference point
        self.dt = params.dt
        self.trajectory = Trajectory2D()

        # variation parameters
        self.control_variation = ControlVariation(params)
        self.x_variation = np.zeros((4, 1))
        self.K = np.array([[1, 0], [0, .5]])

    def calc_input(self, x, vd, wd):
        x_d = np.array([[vd], [wd]])
        v = x[3, 0]
        phi = x[4, 0]
        w = v / self.L * np.tan(phi)
        x_v = np.array([[v], [w]])
        u = -self.K.dot(x_v - x_d)
        return u


if __name__ == '__main__':
    main()
