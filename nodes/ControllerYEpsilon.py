#! /usr/bin/env python
# coding=utf-8

from numpy import eye, array, c_, r_, array, diag, cos, sin, tan
from Parameters import Parameters
from VehicleSim import VehicleSim
import matplotlib.pyplot as plt
import numpy as np
import scipy.linalg
from utils import Trajectory2D
from ControlVariation import ControlVariation


def main():
    # initialization
    params = Parameters()
    controller = ControllerYEpsilon(params)
    sim = VehicleSim(params)
    inner_trajectory = Trajectory2D().load_trajectory("../data/trajectories/inner_path.traj")
    controller.trajectory = controller.trajectory.load_trajectory("../data/trajectories/outter_path.traj")

    x_output = np.zeros((7, 1, len(controller.trajectory.x)))
    u_output = np.zeros((2, 1, len(controller.trajectory.x)))

    phi = np.arctan2(sim.L * inner_trajectory.k[0], 1)
    sim.x = np.array([[inner_trajectory.x[0]],
                      [inner_trajectory.y[0]],
                      [inner_trajectory.theta[0]],
                      [inner_trajectory.v[0]],
                      [phi],
                      [0],
                      [0]])

    time = np.arange(0, len(inner_trajectory.s) * params.dt, params.dt)

    for t in range(len(inner_trajectory.s)):
        # calculate controller input
        u = controller.calc_input(sim.x)

        # propogate vehcile forward
        sim.propagate(u)
        x_output[:, :, t] = sim.x
        u_output[:, :, t] = u

    fig1, ax1 = plt.subplots()
    ax1.plot(x_output[0, :, :].squeeze(), x_output[1, :, :].squeeze())

    fig2, (ax2, ax3) = plt.subplots(2, 1)
    ax2.plot(time, u_output[0, :, :].squeeze())
    ax2.set_ylabel("Accleration")

    ax3.plot(time, u_output[1, :, :].squeeze())
    ax3.set_xlabel("Time")
    ax3.set_ylabel("Steering Rate")

    fig3, ax3 = plt.subplots()
    ax3.plot(time, x_output[3, :, :].squeeze())

    print(sim.x)
    plt.show()


class ControllerYEpsilon:
    def __init__(self, params):
        # vehicle parameters
        self.params = params
        self.L = params.wheel_base  # meters
        self.max_steer_angle = params.max_steer_angle  # phi
        self.max_change_steer_angle = params.max_change_steer_angle  # per second
        self.max_velocity_limit = params.max_velocity_limit  # m/s
        self.max_accel_limit = params.max_accel_limit  # m/s^2

        # control parameters
        self.t = 0
        self.dt = params.dt
        self.eps = params.control_reference  # m from back axel reference point
        self.dt = params.dt
        self.trajectory = Trajectory2D()

        # variation parameters
        self.control_variation = ControlVariation(params)
        self.x_variation = np.zeros((4, 1))

        # initialize matrix variables
        I = eye(2)
        Z = array([[0, 0], [0, 0]])
        A = r_[c_[Z, I], c_[Z, Z]]
        B = r_[Z, I]

        # initialize lqr controller variables
        x_pos = 20
        y_pos = 20
        vel = 1
        omega = 1
        Q = diag([x_pos, y_pos, vel, omega])

        # input penalties
        accel = 1
        ang_accel = 1
        R = diag([accel, ang_accel])
        R_inv = np.linalg.inv(R)

        # calculate feedback matrix
        P = scipy.linalg.solve_continuous_are(A, B, Q, R)
        self.K = R_inv.dot(B.T).dot(P)

    def init_control_variation_noise(self):
        # variation parameters
        self.x_variation = np.zeros((4, 1))

    def set_trajectory(self, trajectory):
        self.trajectory = trajectory
        self.t = 0

    def calc_zero_input(self, x):
        return np.ones((2, 1)) * 0.1

    def calc_input(self, x):
        R_eps = array([[cos(x[2, 0]), -self.eps * sin(x[2, 0])],
                       [sin(x[2, 0]), self.eps * cos(x[2, 0])]])

        w = x[3, 0] * tan(x[4, 0]) / float(self.L)

        w_hat = array([[0, self.eps * w],
                       [w / float(self.eps), 0]])

        y_eps = x[0:2] + self.eps * array([[cos(x[2, 0])],
                                           [sin(x[2, 0])]])

        y_eps_dot = R_eps.dot(array([[x[3, 0]], [w]]))

        # build trajectory matrix

        yd = np.array([[self.trajectory.x[self.t]], [self.trajectory.y[self.t]]])
        yd_dot = np.array([[self.trajectory.x_dot[self.t]], [self.trajectory.y_dot[self.t]]])
        yd_ddot = np.array([[self.trajectory.x_ddot[self.t]], [self.trajectory.y_ddot[self.t]]])

        Z = np.r_[y_eps, y_eps_dot] - np.r_[yd, yd_dot]
        u = yd_ddot - self.K.dot(Z)

        a_bar = np.linalg.inv(R_eps).dot(u) - w_hat.dot(array([[x[3, 0]], [w]]))

        if x[3, 0] == 0:
            xi = 0
        else:
            xi = cos(x[4, 0]) ** 2 * (self.L * a_bar[1, 0] - a_bar[0, 0] * tan(x[4, 0])) / x[3, 0]

        a_bar = array([[a_bar[0, 0]], [xi]])

        self.t = self.t + 1
        return a_bar

    def calc_input_yd(self, x, yd, yd_dot):
        R_eps = array([[cos(x[2, 0]), -self.eps * sin(x[2, 0])],
                       [sin(x[2, 0]), self.eps * cos(x[2, 0])]])

        w = x[3, 0] * tan(x[4, 0]) / float(self.L)

        w_hat = array([[0, self.eps * w],
                       [w / float(self.eps), 0]])

        y_eps = x[0:2] + self.eps * array([[cos(x[2, 0])],
                                           [sin(x[2, 0])]])

        y_eps_dot = R_eps.dot(array([[x[3, 0]], [w]]))

        yd = np.array(yd).reshape(-1, 1)
        yd_dot = np.array(yd_dot).reshape(-1, 1)

        Z = np.r_[y_eps, y_eps_dot] - np.r_[yd, yd_dot]
        u = - self.K.dot(Z)

        a_bar = np.linalg.inv(R_eps).dot(u) - w_hat.dot(array([[x[3, 0]], [w]]))

        if x[3, 0] == 0:
            xi = 0
        else:
            xi = cos(x[4, 0]) ** 2 * (self.L * a_bar[1, 0] - a_bar[0, 0] * tan(x[4, 0])) / x[3, 0]

        a_bar = array([[a_bar[0, 0]], [xi]])
        return a_bar

    def calc_input_vel(self, x, v, w):
        return np.array([[0], [0]])

    def calc_variation_input(self, x):
        R_eps = array([[cos(x[2, 0]), -self.eps * sin(x[2, 0])],
                       [sin(x[2, 0]), self.eps * cos(x[2, 0])]])

        w = x[3, 0] * tan(x[4, 0]) / float(self.L)

        w_hat = array([[0, self.eps * w],
                       [w / float(self.eps), 0]])

        y_eps = x[0:2] + self.eps * array([[cos(x[2, 0])],
                                           [sin(x[2, 0])]])

        y_eps_dot = R_eps.dot(array([[x[3, 0]], [w]]))

        # build trajectory matrix
        alpha, vel, delta = self.control_variation.propagate()

        theta = self.trajectory.theta[self.t] + np.pi / 2.0
        yd = np.array([[self.trajectory.x[self.t]],
                       [self.trajectory.y[self.t]]]) + delta * np.array([[cos(theta)], [sin(theta)]])
        yd_dot = np.array([[self.trajectory.x_dot[self.t]],
                           [self.trajectory.y_dot[self.t]]]) + vel * np.array([[cos(theta)], [sin(theta)]])
        yd_ddot = np.array([[self.trajectory.x_ddot[self.t]],
                            [self.trajectory.y_ddot[self.t]]]) + alpha * np.array([[cos(theta)], [sin(theta)]])

        Z = np.r_[y_eps, y_eps_dot] - np.r_[yd, yd_dot]
        u = yd_ddot - self.K.dot(Z)
        # u = - self.K.dot(Z)

        a_bar = np.linalg.inv(R_eps).dot(u) - w_hat.dot(array([[x[3, 0]], [w]]))

        if x[3, 0] == 0:
            xi = 0
        else:
            xi = cos(x[4, 0]) ** 2 * (self.L * a_bar[1, 0] - a_bar[0, 0] * tan(x[4, 0])) / x[3, 0]

        a_bar = array([[a_bar[0, 0]], [xi]])

        self.t = self.t + 1
        return a_bar


if __name__ == '__main__':
    main()
