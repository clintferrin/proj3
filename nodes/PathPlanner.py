#! /usr/bin/env python
# coding=utf-8

from numpy import eye, array, c_, r_, array, diag, cos, sin, tan
from utils import Trajectory2D, Trajectory1D
import Parameters as pm
import VehicleSim as ve
import numpy as np


def main():
    # initialization
    params = pm.Parameters()
    sim = ve.VehicleSim(params)
    path_planner = PathPlanner(params)


class PathPlanner:
    def __init__(self, params):
        self.__params = params
        self.__t = 0
        self.dt = params.dt
        self.ds = params.ds
        self.__pos_trajectory = Trajectory2D(ds=self.ds)
        self.__v_trajectory = Trajectory1D(ds=self.ds)
        self.__nominal_driving_speed = params.nominal_control_speed
        # self.__nominal_driving_speed = 10
        self.__time_to_nominal = params.time_to_nominal
        self.__end_routine = False

        # populate initial conditions for path planner
        x0 = array([[float(params.x0[0])],
                    [float(params.x0[1])],
                    [float(params.x0[2])],
                    [float(params.x0[3])]])

        # setup initial path trajectory
        x_distance = 100
        y_distance = 100

        self.__pos_trajectory.x = np.arange(x0[0, 0], x_distance, self.ds)
        self.__pos_trajectory.y = np.arange(x0[1, 0], y_distance, self.ds)

        # setup cooresponding velocity trajectory
        self.__v_trajectory = \
            self.__velocity_time_trajectory(x0[3, 0], self.__nominal_driving_speed, self.__time_to_nominal)

    def update_controller_trajectory(self, x):
        v = x[3, 0]
        error = v - self.__get_desired_velocity()
        if abs(error) > self.__params.v_error_epsilon:  # meters per second
            self.__v_trajectory = \
                self.__velocity_time_trajectory(x[3, 0], self.__nominal_driving_speed, self.__time_to_nominal)
            return True

        # if (abs(x[0, 0]) > 90 or abs(x[1, 0]) > 90) and (self.__end_routine is False):
        #     self.__trajectory.velocity = \
        #         self.__velocity_distance_trajectory(v, 10, 100 - max(x[0, 0], x[1, 0]))
        #     self.__trajectory.velocity[-1] = 0
        #     self.__t = 0
        #     self.__end_routine = True
        #     return True

        # if self.__end_routine is True and abs(error) > self.__params.v_error_epsilon:
        #     self.__trajectory.velocity = \
        #         self.__velocity_distance_trajectory(v, 0, 100 - max(x[0, 0], x[1, 0]))
        #     self.__t = 0

        return False

    def get_position_trajectory(self):
        return self.__pos_trajectory

    def get_velocity_trajectory(self):
        return self.__v_trajectory

    def __get_desired_velocity(self):
        if self.__t > len(self.__v_trajectory.path) - 1:
            return self.__v_trajectory.path[-1]
        v_desired = self.__v_trajectory.path[self.__t]
        self.__t = self.__t + 1
        return v_desired

    def __velocity_time_trajectory(self, v_current, v_end, end_time):
        v_trajectory = Trajectory1D(ds=self.ds)
        m = (v_end - v_current) / float(end_time)
        x = np.arange(0, end_time + self.dt, self.dt)
        v_trajectory.path_dot = np.zeros(x.shape) + m
        v_trajectory.path = self.__linear_function(x, m, v_current)
        return v_trajectory

    def __velocity_distance_trajectory(self, v_current, v_end, meters):
        time = 2 * meters / float(v_current)
        return self.__velocity_time_trajectory(v_current, v_end, time)

    def __velocity_slope_trajectory(self, v_current, v_end, slope):
        if (v_current - v_end) != np.sign(slope):
            print("Slope must match velocity direction")
            return np.array([0])

        end_time = (v_end - v_current) / float(slope)
        x = np.arange(v_current, end_time + self.dt, self.dt)
        return self.__linear_function(x, slope, v_current)

    def __linear_function(self, x, m, b):
        return x * m + b


if __name__ == '__main__':
    main()
