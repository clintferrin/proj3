from Parameters import Parameters
from VehicleSim import VehicleSim
import numpy as np
from utils import Trajectory2D
from ControllerVW import ControllerVW
import matplotlib.pyplot as plt


def main():
    params = Parameters()
    controller = ControllerVW(params)
    sim = VehicleSim(params)

    time = np.arange(0, 40, params.dt)
    x_output = np.zeros((7, 1, len(time)))
    u_output = np.zeros((2, 1, len(time)))

    sim.x = x_output[:, :, 0]

    for t in range(len(time)):
        # calculate controller input
        u = controller.calc_input(sim.x, 5, 0.5)

        # propagate vehicle forward
        sim.propagate(u)
        x_output[:, :, t] = sim.x
        u_output[:, :, t] = u

    fig1, ax1 = plt.subplots()
    ax1.plot(x_output[0, :, :].squeeze(), x_output[1, :, :].squeeze())

    fig2, (ax2, ax3) = plt.subplots(2, 1)
    ax2.plot(time, u_output[0, :, :].squeeze())
    ax2.set_ylabel("Acceleration")

    ax3.plot(time, u_output[1, :, :].squeeze())
    ax3.set_xlabel("Time")
    ax3.set_ylabel("Steering Rate")

    # fig3, ax3 = plt.subplots()
    # ax3.plot(time, x_output[3, :, :].squeeze())

    plt.show()


if __name__ == '__main__':
    main()
