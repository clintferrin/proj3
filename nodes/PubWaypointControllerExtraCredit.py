#!/usr/bin/env python
import rospy
import vehicle_sim.msg as vs
from visualization_msgs.msg import Marker, MarkerArray
from Parameters import Parameters
from geometry_msgs.msg import PoseStamped
from ControllerYEpsilon import ControllerYEpsilon
from tf.transformations import euler_from_quaternion
from vehicle_sim.msg import vehicle_inputs, bicycle_state
import numpy as np


def main():
    params = Parameters()
    controller = ControllerYEpsilon(params)
    goals = [[15, 15],
             [0, 30],
             [-10, 5]]
    args = {"controller": controller,
            "goals": goals}
    waypoint_controller = PublishWaypointControl(args)
    waypoint_controller.publish(hz=10)


class PublishWaypointControl:
    def __init__(self, args):
        rospy.init_node('waypoint_follower', anonymous=True)
        rospy.Subscriber("x_state", bicycle_state, self.__x_state_callback)
        self.goals = args["goals"]
        self.num_goals = len(self.goals)
        self.current_goal_idx = 0
        self.controller = args["controller"]
        self.switch_distance = 1

        self.x_hat = np.array([[0.0], [0.0], [0.0], [0.0], [0.0], [0.0], [0.0]])
        self.x_d = 0
        self.y_d = 0
        self.theta_d = 0

    def publish(self, hz=100):
        pub = rospy.Publisher('control_input', vehicle_inputs, queue_size=10)
        rate = rospy.Rate(hz)  # hz
        u_pub = vehicle_inputs()
        yd_dot = [0, 0]

        while not rospy.is_shutdown():
            current_position = [self.x_hat[0, 0], self.x_hat[1, 0]]
            x_d, y_d = self.update_waypoint(current_position)
            yd = [x_d, y_d]

            u = self.controller.calc_input_yd(self.x_hat, yd, yd_dot)
            u_pub.accel = u[0, 0]
            u_pub.change_steering_angle = u[1, 0]

            pub.publish(u_pub)
            rate.sleep()

    def update_waypoint(self, pos):
        x = pos[0]
        y = pos[1]
        x_d = self.goals[self.current_goal_idx][0]
        y_d = self.goals[self.current_goal_idx][1]

        # calculate total distance
        dist = np.sqrt((x - x_d) ** 2 + (y - y_d) ** 2)
        if dist <= self.switch_distance:
            self.current_goal_idx = (self.current_goal_idx + 1) % self.num_goals
        x_d = self.goals[self.current_goal_idx][0]
        y_d = self.goals[self.current_goal_idx][1]

        return x_d, y_d

    def __x_state_callback(self, data):
        self.x_hat[0, 0] = data.x
        self.x_hat[1, 0] = data.y
        self.x_hat[2, 0] = data.psi
        self.x_hat[3, 0] = data.v
        self.x_hat[4, 0] = data.phi
        self.x_hat[5, 0] = data.b_v
        self.x_hat[6, 0] = data.b_phi


if __name__ == '__main__':
    main()
