#! /usr/bin/env python

import numpy as np
from Parameters import Parameters
from VehicleSim import VehicleSim
import KalmanFilter as kf
from SensorSuite import SensorSuite
import matplotlib.pyplot as plt
import utils as ut
from ControllerYEpsilon import ControllerYEpsilon
from MonteCarlo import MonteCarlo
from BatteryModel import BatteryModel
from StochasticVariation import StochasticVariation


def main():
    # initialization
    params = Parameters()
    inner_trajectory = ut.Trajectory2D().load_trajectory("../data/trajectories/inner_path.traj")
    phi = np.arctan2(params.wheel_base * inner_trajectory.k[0], 1)
    params.x0 = [inner_trajectory.x[0],
                 inner_trajectory.y[0],
                 inner_trajectory.theta[0],
                 inner_trajectory.v[0],
                 phi]
    params.end_time = (len(inner_trajectory.s) - 1) * params.dt
    sensors = SensorSuite(params)
    controller = ControllerYEpsilon(params)
    battery = BatteryModel("../data/single_coil.csv", params)
    monte = MonteCarlo(params)
    # load trajectories
    controller.trajectory = controller.trajectory.load_trajectory("../data/trajectories/outter_path.traj")

    # plot output
    save_variation = False
    # prob = params.desired_charge_p  # lower tail test
    prob = ut.confidence_interval_z(params.desired_charge_p)  # CI test
    K = controller.control_variation.set_K_charge(params.desired_charge, prob)
    variation = StochasticVariation(K, K + 1, 2, params)

    sim = []
    observer = []

    # initialize the starting conditions for sim and observer
    for i in range(monte.num_simulations):
        # initialize simulation
        sim.append(VehicleSim(params))
        observer.append(kf.KalmanFilter(sensors, params))

    # increment K and run simulation
    for k in range(variation.k_len):
        controller.control_variation.K = variation.k_range[k]

        # run simulation
        for idx_sim, simulation in enumerate(monte.simulations):
            x = sim[idx_sim].x0
            sim[idx_sim].x = x
            x_hat = observer[idx_sim].x0
            observer[idx_sim].x_hat = x_hat
            controller.t = 0

            for t in range(len(simulation.time)):
                # obtain measurments from current state
                y_tilde = [sensors.get_velocity(x),
                           sensors.get_steering_angle(x)]

                # calculate controller input
                x_hat_control = np.r_[x_hat[0:3, :], np.array([[y_tilde[0]], [y_tilde[1]]])]
                u = controller.calc_input(x_hat_control)

                # propagate the estimated state and covariance
                x_hat, P, resid_P = observer[idx_sim].propagate(y_tilde)

                # discrete update
                if np.mod(t, round(1 / (float(params.gps_freq) * monte.dt))) == 0:
                    x_hat, P, resid, resid_P = observer[idx_sim].update(sensors.get_gps_pos(x))
                    simulation.resid = np.c_[simulation.resid, resid]
                    simulation.resid_t = np.append(simulation.resid_t, t * monte.dt)

                # propogate vehcile forward
                x = sim[idx_sim].propagate(u)

                # update battery charging information
                battery_usage = battery.update_battery_loss(y_tilde[0], u[0], 0, params.dt)
                battery_charge = battery.update_dynamic_charging(x[0, 0], x[1, 0])

                # calculate control variables for plotting
                y_eps = x[0:2] + controller.eps * np.array([[np.cos(x[2, 0])], [np.sin(x[2, 0])]])
                y_eps_hat = x_hat[0:2] + controller.eps * np.array([[np.cos(x_hat[2, 0])], [np.sin(x_hat[2, 0])]])

                # save simulation values
                simulation.x[:, :, t] = x
                simulation.x_hat[:, :, t] = x_hat
                simulation.P[:, :, t] = P
                simulation.resid_P[:, :, t] = resid_P
                simulation.u[:, [t]] = u
                simulation.y_eps[:, :, t] = y_eps
                simulation.y_eps_hat[:, :, t] = y_eps_hat
                simulation.watt_hour_motor_usage[t] = battery_usage
                simulation.watt_hour_charging[t] = battery_charge
                simulation.state_of_charge[t] = battery.state_of_charge

            variation.print_update(k, idx_sim)

        # calc y_error for each simulation
        y_error = calc_y_error(variation, monte, controller)

        # find average 3 sigma error over each time interval
        variation.y_error_three_sig_avg[k] = np.average(np.std(y_error, axis=1) * 3)

        # compute estimated charge given delta
        variation.charge_mu[k], variation.charge_std[k] = calc_charge_profile(monte)

        # variables for plotting functions
        if not save_variation:
            variation.y_error_three_sig[k, 0:] = np.std(y_error, axis=1) * 3

    else:
        # calculate simulation start/end indices for final run
        sim_idx_start = variation.analysis_start_idx
        sim_idx_stop = variation.analysis_start_idx + y_error.shape[0]

        # plot trajectory and 3 sigma
        plot_trajectories(monte, sim_idx_start, sim_idx_stop, variation.k_range[-1])
        # plot_three_sigma(inner_trajectory.x[sim_idx_start:sim_idx_stop], variation.y_error_three_sig[0, :])
        # plot_three_sigma_avg(inner_trajectory.x[sim_idx_start:sim_idx_stop], variation.y_error_three_sig_avg[-1])
        # plt.legend()

        fig2, (ax2, ax3) = plt.subplots(2, 1)
        ax2.plot(monte.time, monte.simulations[0].u[0, :])
        ax2.set_ylabel("Accleration")

        ax3.plot(monte.time, monte.simulations[0].u[1, :])
        ax3.set_xlabel("Time")
        ax3.set_ylabel("Steering Rate")

        fig4, ax4 = plt.subplots()
        ax4.plot(monte.time, monte.simulations[0].x[3, :, :].squeeze())

    # plot desired states
    # plt.plot(controller.trajectory.x, controller.trajectory.y)
    # plt.plot(simulation.y_eps[0, 0, :], simulation.y_eps[1, 0, :])
    # plt.plot(simulation.y_eps_hat[0, 0, :], simulation.y_eps_hat[1, 0, :])
    # plt.plot(simulation.x[0, 0, :], simulation.x[1, 0, :])
    # plt.plot(simulation.x_hat[0, 0, :], simulation.x_hat[1, 0, :])
    # plt.plot(inner_trajectory.x, inner_trajectory.y)
    # inner_trajectory.plot_trajectory_transitions()
    # plt.plot(simulation.time, simulation.u[0, :])
    # plt.plot(simulation.time, simulation.u[1, :])
    # plt.axis('equal')
    plt.show()


def calc_y_error(variation, monte, controller):
    y_error = np.zeros((variation.y_error_three_sig.shape[-1], monte.num_simulations))
    for i in range(monte.num_simulations):
        x_pos = monte.simulations[i].x[0, :, variation.analysis_start_idx:].squeeze()
        y_pos = monte.simulations[i].x[1, :, variation.analysis_start_idx:].squeeze()
        y_error[:, i] = calc_y_error_point(x_pos, y_pos, controller.trajectory)
    return y_error


def calc_y_error_point(x, y, trajectory):
    x_y_pos = np.c_[x, y].T
    # calculate unit vector heading for each point on trajectory
    R = ut.rotation_mat(trajectory.theta)
    x_y_traj = np.c_[trajectory.x, trajectory.y].T
    v_r = np.zeros((2, x_y_traj.shape[-1]))
    unit = np.array([[1], [0]])
    for idx in range(R.shape[-1]):
        v_r[:, [idx]] = R[:, :, idx].dot(unit)

    # calculate y_error from unit vector heading
    # normalize error vector
    v_e_len = x_y_pos.shape[-1]
    v_e = x_y_pos - x_y_traj[:, 0:v_e_len]
    R_90 = ut.rotation_mat(trajectory.theta + np.pi / 2.0)

    y_error = np.zeros(v_e.shape[-1])
    for idx in range(y_error.shape[-1]):
        y_error[idx] = v_e[:, idx].T.dot(R_90[:, :, idx]).dot(v_r[:, idx])

    return y_error


def calc_charge_profile(monte):
    # compute estimated charge given delta
    charge = np.zeros(monte.num_simulations)
    for i in range(monte.num_simulations):
        charge[i] = np.average(monte.simulations[i].watt_hour_charging)
    charge_mu = np.average(charge)
    charge_std = np.std(charge)
    return charge_mu, charge_std


def verify_charge_percent_below_q_d(monte, params):
    fig, ax = plt.subplots()
    charge = np.zeros(monte.num_simulations)
    charge_lt_desired = 0
    for i in range(monte.num_simulations):
        charge[i] = np.average(monte.simulations[i].watt_hour_charging)
        if charge[i] < params.desired_charge:
            charge_lt_desired = charge_lt_desired + 1

    percent_success = 1 - charge_lt_desired / float(monte.num_simulations)
    charge_mu = np.average(charge)
    charge_std = np.std(charge)

    ax.plot(charge, "o")
    ax.set_xlabel("Simulation Number")
    ax.set_ylabel("Expected Charge")

    print("Average Charge: %.3f" % charge_mu)
    print("Standard Deviation of Average Charge: %.3f" % charge_std)
    print("Percent Succesfull Charge: %.3f" % percent_success)

    return percent_success


def verify_delta_max(y_error, params):
    fig, ax = plt.subplots()
    max_errors = np.max(abs(y_error), axis=0)
    num_outside_road = 0
    for max_error in max_errors:
        if max_error < np.sqrt(params.sig2_delta) * 3:
            num_outside_road = num_outside_road + 1

    ax.plot(max_errors, "o")
    print("Numer of simulations outside of desired charge: %d" % num_outside_road)
    ax.set_xlabel("Simulation Number")
    ax.set_ylabel("Maximum Absolute Value of \delta")


def plot_three_sigma(x, three_sig):
    plt.plot(x, three_sig,
             linewidth=0.75, color="red", linestyle="--",
             label="Monte Carlo $3\sigma$")
    plt.plot(x, -three_sig,
             linewidth=0.75, color="red", linestyle="--",
             label="Monte Carlo $3\sigma$")


def plot_three_sigma_avg(x, three_sig_avg):
    plt.plot(x, three_sig_avg * np.ones(x.shape),
             linewidth=0.75, color="k", linestyle="--",
             label="Average Monte Carlo $3\sigma$")
    plt.plot(x, -three_sig_avg * np.ones(x.shape),
             linewidth=0.75, color="k", linestyle="--",
             label="Average Monte Carlo $3\sigma$")


def plot_sim_trajectory(monte, start, stop):
    # plot vehicle trajectory for each simulation
    for i in range(monte.num_simulations):
        x_pos = monte.simulations[i].x[0, :, start:stop].squeeze()
        y_pos = monte.simulations[i].x[1, :, start:stop].squeeze()
        plt.plot(x_pos, y_pos)


def plot_trajectories(monte, start, stop, k_val):
    fig, ax = plt.subplots()
    for i in range(monte.simulations[0].num_simulations):
        x_pos = monte.simulations[i].x[0, :, start:stop].squeeze()
        y_pos = monte.simulations[i].x[1, :, start:stop].squeeze()
        ax.plot(x_pos, y_pos)
    ax.set_title("K=%.4f" % k_val)
    ax.set_xlabel("x position (m)")
    ax.set_ylabel("y position (m)")


def plot_watt_charging(monte, k_val):
    fig, ax = plt.subplots()
    for i in range(monte.simulations[0].num_simulations):
        ax.plot(monte.simulations[i].time, monte.simulations[i].watt_hour_charging)
    ax.set_title("K=%.4f" % k_val)
    ax.set_xlabel("Time (sec)")
    ax.set_ylabel("Charge (Watts)")


if __name__ == '__main__':
    main()
